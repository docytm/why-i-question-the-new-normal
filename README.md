## What this repository contains

It contains the source code for the online article
 discussing my views about the New Normal. The web pages were compiled using Middleman. The Tufte CSS stylesheet was used for formatting the pages (https://edwardtufte.github.io/tufte-css/). I also used plain javascript to
create the "Table of Contents" menu.

My article can be viewed at http://enformtk.u-aizu.ac.jp/en/views/why-i-question-the-new-normal.html


## Install middleman

Install middleman using `gem install middleman`.

Then run `bundle install`

## How to compile the site

Compile the site by typing  `ruby compile_site.rb` on the command line.

## Purpose of the directory named "processed"

The "processed" directory contains a beautified version of the
files under source/en/views.

You may want to use the text from these files, if you are planning
to translate the text from English to a different language.
